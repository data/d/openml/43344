# OpenML dataset: Corporate-Credit-Rating

https://www.openml.org/d/43344

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
A corporate credit rating expresses the ability of a firm to repay its debt to creditors. Credit rating agencies are the entities responsible to make the assessment and give a verdict.  When a big corporation from the US or anywhere in the world wants to issue a new bond it hires a credit agency to make an assessment so that investors can know how trustworthy is the company. The assessment is based especially in the financials indicators that come from the balance sheet. Some of the most important agencies in the world are Moodys, Fitch and Standard and Poors. 
Content
A list of 2029 credit ratings issued by major agencies such as Standard and Poors to big US firms (traded on NYSE or Nasdaq) from 2010 to 2016. 
There are 30 features for every company of which 25 are financial indicators. They can be divided in:

Liquidity Measurement Ratios: currentRatio, quickRatio, cashRatio, daysOfSalesOutstanding
Profitability Indicator Ratios: grossProfitMargin, operatingProfitMargin, pretaxProfitMargin, netProfitMargin, effectiveTaxRate, returnOnAssets, returnOnEquity, returnOnCapitalEmployed
Debt Ratios: debtRatio, debtEquityRatio
Operating Performance Ratios: assetTurnover
Cash Flow Indicator Ratios: operatingCashFlowPerShare, freeCashFlowPerShare, cashPerShare, operatingCashFlowSalesRatio, freeCashFlowOperatingCashFlowRatio

For more information about financial indicators visit: https://financialmodelingprep.com/market-indexes-major-markets
The additional features are Name, Symbol (for trading), Rating Agency Name, Date and Sector. 
The dataset is unbalanced, here is the frequency of ratings:

AAA:         7
AA:            89
A:           398
BBB:           671
BB:           490
B:           302
CCC:       64
CC:           5
C:           2
D:           1

Acknowledgements
This dataset was possible thanks to financialmodelingprep and opendatasoft - the sources of the data. To see how the data was integrated and reshaped check here.
Inspiration
Is it possible to forecast the rating an agency will give to a company based on its financials?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43344) of an [OpenML dataset](https://www.openml.org/d/43344). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43344/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43344/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43344/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

